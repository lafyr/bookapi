<?php

use App\BookApi\Models\Book;

/**
 * Class BooksTest
 */
class BooksTest extends ApiTester
{
    /** @test */
    public function it_returns_books()
    {
        $this->makeBooks();

        $this->getJson('api/books');

        $this->assertResponseOk();
    }

    /** @test */
    public function it_returns_one_book()
    {
        $this->makeBooks();

        $book = $this->getJson('api/books/1')->data;

        $this->assertResponseOk();

        $this->assertObjectHasAttributes($book, 'title', 'author', 'publisher', 'price', 'available');

    }

    /** @test */
    public function it_returns_404_on_lesson_not_found()
    {
        $this->getJson('api/books/1');

        $this->assertResponseStatus(404);
    }

    /**
     * @param array $bookFields
     */
    private function makeBooks($bookFields = [])
    {
        $book = array_merge([
            'title' => $this->fake->sentence(4),
            'author' => $this->fake->name,
            'publisher' => $this->fake->company,
            'price' => $this->fake->randomNumber(4, true),
            'available' => $this->fake->boolean()
        ], $bookFields);

       while($this->times--) Book::create($book);
    }


}
