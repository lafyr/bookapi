<?php

/**
 * Created by PhpStorm.
 * User: Pedro
 */

use Faker\Factory as Faker;


/**
 * Class ApiTester
 */
class ApiTester extends TestCase
{

    /**
     * @var \Faker\Generator
     */
    protected $fake;
    /**
     * @var int
     */
    protected $times = 1;

    /**
     * ApiTester constructor.
     * @internal param $fake
     */
    public function __construct()
    {
        $this->fake = Faker::create();
    }

    /**
     *  Run our migrations on test setup
     */
    public function setUp()
    {
        parent::setUp();

        Artisan::call('migrate');
    }

    /**
     * @param $count
     * @return $this
     */
    protected function repeat($count)
    {
        $this->times = $count;

        return $this;
    }

    /**
     * @param $uri
     * @return mixed
     */
    protected function getJson($uri)
    {
        return json_decode($this->call('GET', $uri)->getContent());
    }

    /**
     * Pass through an object followed by an array of attributes and
     * call assertObjectHasAttribute on each
     */
    protected function assertObjectHasAttributes()
    {
        $args = func_get_args();
        $object = array_shift($args);

        foreach ($args as $attribute) {
            $this->assertObjectHasAttribute($attribute, $object);
        }
    }
}