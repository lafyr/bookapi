<?php
/**
 * Created by PhpStorm.
 * User: Pedro
 */

namespace App\BookApi\Transformers;


/**
 * Class BookTransformer
 * @package App\BookApi\Transformers
 */
class BookTransformer extends Transformer
{
    /**
     * @param $book
     * @return array
     */
    public function transform($book)
    {
        return [
            'title' => $book['title'],
            'author' => $book['author'],
            'publisher' => $book['publisher'],
            'price' => number_format(($book['price'] / 100), 2),
            'available' => (boolean)$book['available']
        ];
    }
}