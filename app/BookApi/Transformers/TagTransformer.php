<?php
/**
 * Created by PhpStorm.
 * User: Pedro
 */

namespace App\BookApi\Transformers;


/**
 * Class TagTransformer
 * @package App\BookApi\Transformers
 */
class TagTransformer extends Transformer
{
    /**
     * @param $tag
     * @return array
     */
    public function transform($tag)
    {
        return [
            'name' => $tag['name']
        ];
    }
}