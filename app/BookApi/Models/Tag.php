<?php

namespace App\BookApi\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tag
 * @package App\BookApi\Models
 */
class Tag extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name'];

}
