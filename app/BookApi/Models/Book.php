<?php

namespace App\BookApi\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Book
 * @package App\BookApi\Models
 */
class Book extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title', 'author', 'publisher', 'price', 'available'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::Class);
    }
}
