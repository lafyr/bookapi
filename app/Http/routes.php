<?php

Route::get('/', function () {
    return redirect()->route('api.books.index');
});


Route::group(['prefix' => 'api/', 'middleware' => ['api']], function () {
    Route::get("books/{id}/tags", "TagsController@index");
    Route::resource('books', 'BooksController');
    Route::resource('tags', 'TagsController', ["only" => ['index', 'show']]);
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
});
