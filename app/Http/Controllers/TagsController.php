<?php

namespace App\Http\Controllers;

use App\BookApi\Models\Book;
use App\BookApi\Models\Tag;
use App\BookApi\Transformers\TagTransformer;
use App\Http\Requests;

/**
 * Class TagsController
 * @package App\Http\Controllers
 */
class TagsController extends ApiController
{

    /**
     * @var TagTransformer
     */
    protected $tagTransformer;

    /**
     * TagsController constructor.
     * @param $tagTransformer
     */
    public function __construct(TagTransformer $tagTransformer)
    {
        $this->tagTransformer = $tagTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @param null $bookID
     * @return \Illuminate\Http\Response
     */
    public function index($bookID = null)
    {
        $tags = $this->getTags($bookID);

        return $this->respond([
            'data' => $this->tagTransformer->transformCollection($tags->all())
        ]);
    }

    /**
     * @param $bookID
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function getTags($bookID)
    {
        return $bookID ? Book::findOrFail($bookID)->tags : Tag::all();
    }
}
