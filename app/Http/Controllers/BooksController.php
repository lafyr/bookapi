<?php

namespace App\Http\Controllers;

use App\BookApi\Models\Book;
use App\BookApi\Transformers\BookTransformer;
use Illuminate\Http\Request;
use App\Http\Requests;

class BooksController extends ApiController
{
    /**
     * @var bookTransformer
     */
    protected $bookTransformer;

    /**
     * BooksController constructor.
     * @param BookTransformer $bookTransformer
     */
    public function __construct(BookTransformer $bookTransformer)
    {
        $this->bookTransformer = $bookTransformer;
        $this->middleware('auth.basic', ['only' => 'store']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();

        return $this->respond([
            'data' => $this->bookTransformer->transformCollection($books->all())
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'title' => 'required|string',
            'author' => 'required|string',
            'publisher' => 'required|string',
            'price' => 'required|numeric',
            'available' => 'required|boolean',
        ];

        $this->validate($request, $rules);

        $book = Book::create($request->all());

        return $this->respondCreated('Book created successfully', $book);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::find($id);

        if (!$book) {

            return $this->respondNotFound('Book does not exist');
        }

        return $this->respond([
            'data' => $this->bookTransformer->transform($book)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
