<?php

use Illuminate\Database\Seeder;

class BookTagTableSeeder extends Seeder
{
    /**
     * Seed the reviews table.
     *
     * @return void
     */
    public function run()
    {
        $bookIds = DB::table('books')->lists('id');
        $tagIds = DB::table('tags')->lists('id');

        foreach ($bookIds as $bookid) {

            $unusedTags = $tagIds;
            shuffle($unusedTags);

            foreach (range(1,3) as $tags) {
                DB::table('book_tag')->insert([
                    'book_id' => $bookid,
                    'tag_id' => array_shift($unusedTags),
                ]);
            }
        }
    }
}