<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\BookApi\Models\Book;

class BooksTableSeeder extends Seeder
{
    /**
     * Seed the books table.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 50) as $item) {
            Book::create([
                'title' => $faker->sentence(4),
                'author' => $faker->name,
                'publisher' => $faker->company,
                'price' => $faker->randomNumber(4, true),
                'available' => $faker->boolean()
            ]);
        }
    }
}
