<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\BookApi\Models\Tag;

class TagsTableSeeder extends Seeder
{
    /**
     * Seed the reviews table.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 50) as $item) {
            Tag::create([
                'name' => $faker->word,
            ]);
        }
    }
}
